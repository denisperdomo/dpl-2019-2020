var http = require('http');
var port = 3000;

http.createServer(function (req, res) {
	res.writeHead(200, {'Content-Type': 'text/html'});
	console.log(`Servidor corriendo en el puerto ${port}`)
	res.end('Ejercicio 3 - Actividad A2 - Denis Perdomo');
}).listen(port); 
